function mapsSelector() {
    if /* if we're on iOS, open in Apple Maps */ ((navigator.platform.indexOf("iPhone") != -1) ||
        (navigator.platform.indexOf("iPad") != -1) ||
        (navigator.platform.indexOf("iPod") != -1))
        window.open("maps://maps.google.com/maps?daddr=Suokatu+41,Kuopio");
    else /* else use Google */
        window.open("https://maps.google.com/maps?daddr=Suokatu+41,Kuopio");
}


$(function() {

    var d = new Date();
    var weekday = new Array(7);
    weekday[0] = "su";
    weekday[1] = "mato";
    weekday[2] = "mato";
    weekday[3] = "mato";
    weekday[4] = "mato";
    weekday[5] = "pe";
    weekday[6] = "la";

    var n = weekday[d.getDay()];

    $('#' + n).addClass('today');
})

$(function() {
    var header = document.querySelector("#mainNav");

    if (window.location.hash) {
        header.classList.add("headroom--unpinned");
    }

    var headroom = new Headroom(header, {
        tolerance: {
            down: 30,
            up: 30
        },
        offset: 205
    });
    headroom.init();

})